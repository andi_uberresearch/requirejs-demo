# [RequireJS](requirejs.org) Demo

This is a small requirejs demo build via gulp as a task runner.

## Installation

Run `npm install` and `bower install`.

Then run `gulp requirejs`.

That is it! Now open the index.html inside your browser and watch the network panel!
