module.exports = {
  appDir: './tmp',
  baseUrl: "./",
  mainConfigFile: './tmp/config.js',
  dir: './dist',
  modules: [
    {
      name: 'app/main'
    }
  ]
}