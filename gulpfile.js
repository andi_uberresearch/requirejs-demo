var gulp = require('gulp');
var clean = require('gulp-clean');

gulp.task('bowercopy', ['cleantmp'], function(){
  var mainBowerFiles = require('main-bower-files');
  return gulp.src(mainBowerFiles()).pipe(gulp.dest('./tmp'));
});

gulp.task('copy', ['cleantmp'], function(){
  return gulp.src('./src/**/*js').pipe(gulp.dest('./tmp'));
});

gulp.task('cleantmp', function(){
  return gulp.src('./tmp').pipe(clean());
});

gulp.task('cleandist', function(){
  return gulp.src('./dist').pipe(clean());
});

gulp.task('requirejs', ['bowercopy', 'copy', 'cleandist'], function () {
  var rjs = require('requirejs');
  var rjsconfig = require('./rconfig.js');
  rjs.optimize(rjsconfig);
});
